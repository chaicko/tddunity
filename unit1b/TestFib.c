
#include "unity_fixture.h"
#include "Fib.h"
#include <string.h>

#define DIMENSION_OF(a) (sizeof(a) / sizeof(a[0]))

/* ----- Fibonacci_GetElement ----- */
TEST_GROUP(Fibonacci_GetElement);

TEST_SETUP(Fibonacci_GetElement)
{
}

TEST_TEAR_DOWN(Fibonacci_GetElement)
{
}

TEST(Fibonacci_GetElement, TestElements01)
{
  TEST_ASSERT_EQUAL_INT(1, Fibonacci_GetElement(0));
  TEST_ASSERT_EQUAL_INT(1, Fibonacci_GetElement(1));
}

TEST(Fibonacci_GetElement, TestOtherPartsOfTheSequence)
{
  TEST_ASSERT_EQUAL_INT(3, Fibonacci_GetElement(3));
  TEST_ASSERT_EQUAL_INT(8, Fibonacci_GetElement(5));
  TEST_ASSERT_EQUAL_INT(89, Fibonacci_GetElement(10));
}

TEST(Fibonacci_GetElement, TestSeveralElements)
{
  int expected[] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144};
  int i;
  for(i = 0; i < DIMENSION_OF(expected); i++) {
    TEST_ASSERT_EQUAL_INT( expected[i], Fibonacci_GetElement(i));
  }
}

TEST(Fibonacci_GetElement, TestNegativeElementsReturn0)
{
  TEST_ASSERT_EQUAL_INT( 0, Fibonacci_GetElement(-1));
  TEST_ASSERT_EQUAL_INT( 0, Fibonacci_GetElement(-10));
  TEST_ASSERT_EQUAL_INT( 0, Fibonacci_GetElement(INT_MIN));
}

TEST(Fibonacci_GetElement, TestOverrunValues)
{
  const int first_bad_element = 46;
  const int last_good_element = first_bad_element-1;
  int last_good_value = Fibonacci_GetElement(last_good_element);

  TEST_ASSERT_EQUAL_INT_MESSAGE(4, sizeof(int), "Constants first_bad_element and FIRST_MAX_ELEMENT should be changed");
  TEST_ASSERT_MESSAGE( last_good_value > 1, "This value should not have been rollover");
  TEST_ASSERT_EQUAL_INT( 0, Fibonacci_GetElement(first_bad_element));
  TEST_ASSERT_EQUAL_INT( 0, Fibonacci_GetElement(INT_MAX));
}

/* ----- Fibonacci_IsInSequence ----- */

TEST_GROUP(Fibonacci_IsInSequence);

TEST_SETUP(Fibonacci_IsInSequence)
{
}

TEST_TEAR_DOWN(Fibonacci_IsInSequence)
{
}

TEST(Fibonacci_IsInSequence, TestElementsFound)
{
  const int first_bad_element = 46;
  int element_in_sequence, i;
  char str[50];

  for(i = 0; i < first_bad_element; i++)
  {
    element_in_sequence = Fibonacci_GetElement(i);
    sprintf(str, "Element[%d]=%d was not found in sequence", i, element_in_sequence);
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, Fibonacci_IsInSequence(element_in_sequence), str);
  }
}

TEST(Fibonacci_IsInSequence, TestLessOrEqualZeroAreNotInSequence)
{
  TEST_ASSERT_EQUAL_INT(0, Fibonacci_IsInSequence(0));
  TEST_ASSERT_EQUAL_INT(0, Fibonacci_IsInSequence(-1));
  TEST_ASSERT_EQUAL_INT(0, Fibonacci_IsInSequence(INT_MIN));
}

TEST(Fibonacci_IsInSequence, TestValuesNotInSequence)
{
  int numbers_not_in_fib_sequence[] = {4, 6, 7, 10, 17, 30, 40, 50, 90, 100, 120, 130};
  int i;
  for(i = 0; i < DIMENSION_OF(numbers_not_in_fib_sequence); i++) {
    TEST_ASSERT_EQUAL_INT( 0, Fibonacci_IsInSequence(numbers_not_in_fib_sequence[i]));
  }
}

/* ----- Runner  ----- */

TEST_GROUP_RUNNER(Fibonacci_GetElement)
{
    RUN_TEST_CASE(Fibonacci_GetElement, TestElements01);
    RUN_TEST_CASE(Fibonacci_GetElement, TestOtherPartsOfTheSequence);
    RUN_TEST_CASE(Fibonacci_GetElement, TestSeveralElements);
    RUN_TEST_CASE(Fibonacci_GetElement, TestNegativeElementsReturn0);
    RUN_TEST_CASE(Fibonacci_GetElement, TestOverrunValues);
}

TEST_GROUP_RUNNER(Fibonacci_IsInSequence)
{
    RUN_TEST_CASE(Fibonacci_IsInSequence, TestElementsFound);
    RUN_TEST_CASE(Fibonacci_IsInSequence, TestLessOrEqualZeroAreNotInSequence);
    RUN_TEST_CASE(Fibonacci_IsInSequence, TestValuesNotInSequence);
}

static void runAllTests(void)
{
    RUN_TEST_GROUP(Fibonacci_GetElement);
    RUN_TEST_GROUP(Fibonacci_IsInSequence);
}

int main(int argc, const char* argv[])
{
    return UnityMain(argc, argv, runAllTests);
}
