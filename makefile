#We try to detect the OS we are running on, and adjust commands as needed
ifeq ($(OSTYPE),cygwin)
	CLEANUP = rm -f
	MKDIR = mkdir -p
	TARGET_EXTENSION=.out
else ifeq ($(OS),Windows_NT)
	CLEANUP = del /F /Q
	MKDIR = mkdir
	TARGET_EXTENSION=.exe
else
	CLEANUP = rm -f
	MKDIR = mkdir -p
	TARGET_EXTENSION=.out
endif

all: unit1a unit1b unit2 unit3a unit3b unit3c

unit1a:
	$(MAKE) -C ./unit1a all

unit1b:
	$(MAKE) -C ./unit1b all

unit2:
	$(MAKE) -C ./unit2 all

unit3a:
	$(MAKE) -C ./unit3a all

unit3b:
	$(MAKE) -C ./unit3b all

unit3c:
	$(MAKE) -C ./unit3c all

.PHONY: unit1a
.PHONY: unit1b
.PHONY: unit2
.PHONY: unit3a
.PHONY: unit3b
.PHONY: unit3c
.PHONY: all