
#include "unity.h"

void test_TheFirst(void)
{
  TEST_ASSERT(1==1);
}

void test_TheSecond(void)
{
    TEST_IGNORE_MESSAGE("Implement Me");
}

void test_TheThird(void)
{
    TEST_IGNORE_MESSAGE("Implement Me");
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_TheFirst);
    RUN_TEST(test_TheSecond);
    RUN_TEST(test_TheThird);
    return UNITY_END();
}
